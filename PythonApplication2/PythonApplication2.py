
from abc import ABC, abstractmethod
# Найти сумму элементов списка, расположенных между первым и последним нулевыми
# элементами.  Найти количество таких элементов.
class BaseStrategy(ABC):
    @abstractmethod
    def do_work(self,s):
        pass

class Adder(BaseStrategy):
    def do_work(self, ns):
        self.sum = 0
        for i in ns:
            if i > 0:
                self.sum+=i
        return self.sum

class Multiplicator(BaseStrategy):
    def do_work(self,ns):
        self.proizv = 0
        for i in ns:
            if i > 0:
                self.proizv*=i
        return proizv.sum


class Spisok:
    def __init__(self, n):
        self.s = []

        for i in range(n):
            try:
                self.s.append(int(input(f'Введите элемент списка: ')))
            except:
                print('Вы пытались вводить не целое число!')

    def get_keys_zero(self):
        list_zero_key = []
        for key, value in enumerate(self.s):
            if (value == 0):
                list_zero_key.append(key)
        return list_zero_key

    def count_items(self):
        count = 0
        for key in range(self.get_keys_zero()[-1]):
            if (key != 0):
                count += 1

        return count
            
    def summa_last_zero(self):
        sum = 0
        for key in range(self.get_keys_zero()[-1]):
            if (key != 0):
                sum += self.s[key]

        return sum
        
    def get_spisok(self):
        return self.s

class Calculator:
           
    def set_strategy(self, strategy: BaseStrategy):
        self.strategy = strategy
         
    def calculate(self,ns):
        print('Результат этой стратегии:', self.strategy.do_work(ns))    



s = Spisok(5)
print(f'количество элементов между первым и последним нулевыми элементами: {s.count_items()}')
print(f'сумма элементов между первым и последним нулевыми элементами: {s.summa_last_zero()}')

spi = s.get_spisok()
calc = Calculator()
calc.set_strategy(Adder())
calc.calculate(spi)